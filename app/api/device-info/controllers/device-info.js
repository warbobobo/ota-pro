'use strict';
const { fromJS } = require('immutable');
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async create(ctx) {
    const { model, packageName }= ctx.request.body;
    const burn_img = await strapi.services['burn-img'].findOne({ model });
    const firmware = await strapi.services.firmware.findOne({ model });
    const application = await strapi.services.application.findOne({ packageName });
    const json = {
      sn: ctx.request.body.sn,
      imsi: ctx.request.body.imsi,
      fingerPrint: ctx.request.body.fingerPrint,
      burn_img: burn_img._id,
      firmware: firmware._id,
      application: application._id,
      systemVersion:'',
      appVersion: ''
    }
    return await strapi.services['device-info'].create(json);
  },
  async update(ctx) {
    const json = {};
    const { sn } = ctx.params;
    const { model, packageName, fingerPrint, systemVersion, appVersion }= ctx.request.body;
    const info = await strapi.services['device-info'].findOne({ sn });
    if(model){
      const burn_img = await strapi.services['burn-img'].findOne({model});
      const firmware = await strapi.services.firmware.findOne({model});
      json.burn_img = burn_img._id;
      json.firmware = firmware._id;
    }
    if(packageName){
      const application = await strapi.services.application.findOne({packageName});
      json.application = application._id;
    }
    if(fingerPrint) {
      json.fingerPrint = fingerPrint;
    }
    if(systemVersion) {
      json.systemVersion = systemVersion;
    }
    if(appVersion) {
      json.appVersion = appVersion;
    }
    return await strapi.services['device-info'].update({ _id: info._id }, json);
  },
  async version(ctx) {
    const data = await strapi.services['device-info'].findOne({ sn: ctx.params.sn });

    const json = {
      sn: data.sn,
      imsi: data.imsi,
      burn_img: {
        model: data.burn_img.model,
        buildName: data.burn_img.buildName,
        files: [],
        version: ""
      },
      firmware: {
        model: data.firmware.model,
        buildName: data.firmware.buildName,
        files: [],
        version: ""
      },
      application: {
        packageName: data.application.packageName,
        files: [],
        version: ""
      }
    };

    const burn_imgFile = data.burn_img.versions.sort((a,b) => {return  a.version < b.version ? 1 : -1})[0];
    const firmwareFile = data.firmware.versions.sort((a,b) => {return  a.version < b.version ? 1 : -1})[0];
    const applicationFile = data.application.versions.sort((a,b) => {return  a.version < b.version ? 1 : -1})[0];
    if(burn_imgFile) {
      json.burn_img.version = burn_imgFile.version;
      json.burn_img.files = burn_imgFile.source
    }

    if(firmwareFile) {
      json.firmware.version = firmwareFile.version;
      json.firmware.files = firmwareFile.source
    }

    if(applicationFile) {
      json.application.version = applicationFile.version;
      json.application.files = applicationFile.source
    }
    return json;
  }
};
