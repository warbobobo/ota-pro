'use strict';

/**
 * Lifecycle callbacks for the `device-info` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    const json = {
      sn: attrs.sn,
      imsi: attrs.imsi
    };
    const data = strapi.services['device-status'].create(json);
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  afterUpdate: async (model, attrs, options) => {
    if(!model._conditions._id){ console.log('device-info-id', model._conditions._id); return;}
    const data = await strapi.services['device-info'].findOne({ id: model._conditions._id });
    const json = {
      sn: data.sn,
      imsi: data.imsi,
      fingerPrint: data.fingerPrint,
      systemVersion: data.systemVersion,
      appVersion: data.appVersion
    }
    let status = await strapi.services['device-status'].find({ sn: data.sn})
    if(status.length == 0){
      status = await strapi.services['device-status'].find({ imsi: data.imsi})
    }
    await strapi.services['device-status'].update({ _id: status[0]._id }, json);
  },

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
