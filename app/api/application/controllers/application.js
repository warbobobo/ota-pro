'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

  
module.exports = {
  // async find(ctx) {
  //     const data = await strapi.services.application.find(ctx.query);
  //     // console.log(data[0]);
  //     const json = {
  //       versions: []
  //     };
  //     json.versions = data[0].versions;
  //     // json.versions[2].source = data[0].versions[0].source;
  //     // json.versions[2].version = data[0].versions[0].version;
  //     // // // json.packageName = "tttt";
  //     // // // json.versions =data[0].versions;
      
  //     // await strapi.services.application.update({ _id: data[0].id }, json);
  //     // await strapi.services.application.create(json);
  //     return data;
  // },
  async find(ctx) {
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services.application.search(ctx.query);
    } else {
      entities = await strapi.services.application.find(ctx.query);
    }

    return entities.map(entity => {
      const application = sanitizeEntity(entity, {
        model: strapi.models.application,
      });
      // if (restaurant.chef && restaurant.chef.email) {
      //   delete restaurant.chef.email;
      // }
      // delete application.test;
      delete application.versions;
      delete application.device_infos;
      console.log('application', application);
      // return {
      //   _id: application._id,
      //   version: application.version,
      //   packageName: application.packageName,
      //   // versions: application.versions,
      //   device_infos: application.device_infos
      // };
      return application;
    });
  },
  async updateFile(ctx) {
    const { packageName } = ctx.params;
    const { version, source }= ctx.request.body;
    const application = await strapi.services.application.findOne({packageName});
    const json = { versions:[] }
    for(let i=0;i<application.versions.length;i++) {
      const sources = [];
      for(let j=0;j<application.versions[i].source.length;j++) {
        sources.push(application.versions[i].source[j].id);
      }
      json.versions.push({
        version: application.versions[i].version,
        source: sources
      });
    }
    json.versions.push({
      version: version,
      source: source
    });
    return await strapi.services.application.update({ _id: application.id }, json);
  },
  // async updateFile(ctx){
  //   const { packageName } = ctx.params;
  //   const { data, files } = parseMultipartData(ctx);
  //   console.log('files', files);
  //   console.log('data', data);
  //   console.log('packageName', packageName);
  //   // const { packageName, source, version } = ctx.params;
  //   const application = await strapi.services.application.findOne({packageName});
  //   const json = { versions:[] }
  //   // console.log('length', application.versions.length);
  //   for(let i=0;i<application.versions.length;i++) {
  //     const sources = [];
  //     for(let j=0;j<application.versions[i].source.length;j++) {
  //       sources.push(application.versions[i].source[j].id);
  //     }
  //     json.versions.push({
  //       version: application.versions[i].version,
  //       source: sources
  //     });
  //   }
  //   json.versions.push({
  //     version: data.version,
  //     // source: files
  //   });
  //   // const json = { 
  //   //   versions: {
  //   //     version: data.version
  //   //   } 
  //   // }
  //   console.log('json', json);
    
  //   return await strapi.services.application.update({ id: application.id }, json);
  //   // return await strapi.services.application.update({ id: application.id }, json, { files });
  //   // if (ctx.is('multipart')) {
  //   //   const { data, files } = parseMultipartData(ctx);
  //   //   entity = await service.update({ id: application.id }, data, { files });
  //   // } else {
  //   //   entity = await service.update({ id: ctx.params.id }, ctx.request.body);
  //   // }
  // },
  async create(ctx) {
    console.log('application create');
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      console.log('files', files);
      return await strapi.services.application.create(data, { files });
    } else {
      return await strapi.services.application.create(ctx.request.body);
    }
  }
};
