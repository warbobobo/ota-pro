'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async updateFile(ctx) {
    const { model } = ctx.params;
    const { version, source }= ctx.request.body;
    const burnImg = await strapi.services['burn-img'].findOne({model});
    const json = { versions:[] }
    for(let i=0;i<burnImg.versions.length;i++) {
      const sources = [];
      for(let j=0;j<burnImg.versions[i].source.length;j++) {
        sources.push(burnImg.versions[i].source[j].id);
      }
      json.versions.push({
        version: burnImg.versions[i].version,
        source: sources
      });
    }
    json.versions.push({
      version: version,
      source: source
    });
    return await strapi.services['burn-img'].update({ _id: burnImg.id }, json);
  }
};
